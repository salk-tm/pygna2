Input files
===========

.. role:: zsh(code)
   :language: zsh


Network file
------------
:zsh:`network.tsv`, a tab-delimited file with two columns. Each row is a pair of co-expressed genes. For example:

.. code-block:: none

   Glyma.01G075300	Glyma.01G068000
   Glyma.01G076100	Glyma.01G074800
   Glyma.02G148200	Glyma.02G024200
   Glyma.02G148500	Glyma.01G163500
   Glyma.02G149600	Glyma.02G024200
   Glyma.02G149600	Glyma.02G148200


.. note::

   The input file can contain additional columns (such as a Cytoscape edges file), but they will be ignored.


Gene set file
-------------

:zsh:`genesets.gmt`, a tab-delimited file with one row per gene set. Each row is a gene set name, followed by a description, followed by a tab-separated list of genes. This format is called GMT for "Gene Matrix Transposed". For example:

.. code-block:: none

   cortex	"genes expressed in cortex"	Glyma.01G075300	Glyma.09G115800	Glyma.12G116800	...
   stele	"genes expressed in stele"	Glyma.01G075300	Glyma.09G115800	Glyma.12G116800	...
