PyGNA2 documentation
====================

.. role:: zsh(code)
   :language: zsh

Overview
--------

.. automodule:: pygna2

Source code
-----------

See source code for PyGNA2 at `https://gitlab.com/salk-tm/pygna2 <https://gitlab.com/salk-tm/pygna2>`_.

.. toctree::

   installation
   inputs
   examples
   reference



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
