Installation
============

.. role:: zsh(code)
   :language: zsh

Create and activate a conda environment with :zsh:`pygna` by:

.. code-block:: zsh

   conda create -n pygna2 -c stracquadaniolab -c bioconda \
     -c conda-forge pygna "numpy<1.20"
   conda activate pygna2

Then install :zsh:`pygna2` with pip

.. code-block:: zsh

   pip install pygna2

Check that the installation was successful by running one of:

.. code-block:: zsh

   pygna2 --version
   pygna2 --help
   pygna2
